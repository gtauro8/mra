package tdd.training.mra;

import java.util.List;

public class MarsRover {

	public int pX;
	public int pY;
	public String roverDir = "";

	public String getRoverDir() {
		return roverDir;
	}

	public void setRoverDir(String roverDir) {
		this.roverDir = roverDir;
	}

	public List<String> obstacles = null;

	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {

		this.pX = planetX;
		this.pY = planetY;
		this.obstacles = planetObstacles;
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {

		String anObstacle = x + "," + y;

		return obstacles.contains(anObstacle);

	}

	private void MultiCommands(String s) throws MarsRoverException {

		for (int i = 0; i < s.length(); i++) {

			executeCommand(String.valueOf(s.charAt(i)));
		}

	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {

		if (commandString == " ") {
			return pX + "," + pY + "," + getRoverDir();
		}

		if (commandString.length() > 1) {
			MultiCommands(commandString);
		}

		if (getRoverDir().equals("N")) {
			if (commandString.equals("f")) {
				pY++;
			} else if (commandString.equals("b")) {
				if (pY == 0) {
					pY = 9;
				} else {
					pY--;
				}
			}

			if (commandString.equals("l")) {
				setRoverDir("W");
			} else if (commandString.equals("r")) {
				setRoverDir("E");
			}

		} else if (getRoverDir().equals("W")) {
			if (commandString.equals("f")) {
				if (pX == 0) {
					pX = 9;
				} else {
					pX--;
				}
			} else if (commandString.equals("b")) {
				pX++;
			}

			if (commandString.equals("l")) {
				setRoverDir("S");
			} else if (commandString.equals("r")) {
				setRoverDir("N");
			}

		} else if (getRoverDir().equals("S")) {
			if (commandString.equals("f")) {
				if (pY == 0) {
					pY = 9;
				} else {
					pY--;
				}
			} else if (commandString.equals("b")) {
				pY++;
			}
			if (commandString.equals("l")) {
				setRoverDir("E");
			} else if (commandString.equals("r")) {
				setRoverDir("W");
			}

		} else if (getRoverDir().equals("E")) {
			if (commandString.equals("f")) {
				pX++;
			} else if (commandString.equals("b")) {
				if (pX == 0) {
					pX = 9;
				} else {
					pX--;
				}
			}

			if (commandString.equals("l")) {
				setRoverDir("N");
			} else if (commandString.equals("r")) {
				setRoverDir("S");
			}
		}

		return pX + "," + pY + "," + getRoverDir();
	}

}
