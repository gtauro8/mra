package tdd.training.mra;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class MarsRoverTest {

	List<String> obstacles = new ArrayList<>();

	@Test
	public void hereWeHaveAnObstacle() throws MarsRoverException {

		int pX = 10;
		int pY = 10;

		obstacles.add("5,5");
		obstacles.add("4,7");
		MarsRover rover = new MarsRover(pX, pY, obstacles);

		assertTrue(rover.planetContainsObstacleAt(5, 5));
	}

	@Test
	public void emptyStringLikeCommand() throws MarsRoverException {

		int pX = 0;
		int pY = 0;
		obstacles.add("5,5");
		obstacles.add("4,7");
		MarsRover rover = new MarsRover(pX, pY, obstacles);
		rover.setRoverDir("N");
		assertEquals("0,0,N", rover.executeCommand(" "));

	}

	@Test
	public void turnLeftCommand() throws MarsRoverException {

		int pX = 0;
		int pY = 0;
		obstacles.add("5,5");
		obstacles.add("4,7");
		MarsRover rover = new MarsRover(pX, pY, obstacles);
		rover.setRoverDir("N");
		assertEquals("0,0,W", rover.executeCommand("l"));

	}

	@Test
	public void turnRightCommand() throws MarsRoverException {

		int pX = 0;
		int pY = 0;
		obstacles.add("5,5");
		obstacles.add("4,7");
		MarsRover rover = new MarsRover(pX, pY, obstacles);
		rover.setRoverDir("N");
		assertEquals("0,0,E", rover.executeCommand("r"));

	}

	@Test
	public void moveForwardCommand() throws MarsRoverException {

		int pX = 0;
		int pY = 0;
		obstacles.add("5,5");
		obstacles.add("4,7");
		MarsRover rover = new MarsRover(pX, pY, obstacles);
		rover.setRoverDir("N");
		assertEquals("0,1,N", rover.executeCommand("f"));

	}

	@Test
	public void moveBackwardCommand() throws MarsRoverException {

		int pX = 0;
		int pY = 1;
		obstacles.add("5,5");
		obstacles.add("4,7");
		MarsRover rover = new MarsRover(pX, pY, obstacles);
		rover.setRoverDir("N");
		assertEquals("0,0,N", rover.executeCommand("b"));

	}

	@Test
	public void executionOfMultiCommand() throws MarsRoverException {

		int pX = 0;
		int pY = 0;
		obstacles.add("5,5");
		obstacles.add("4,7");
		MarsRover rover = new MarsRover(pX, pY, obstacles);
		rover.setRoverDir("N");
		assertEquals("2,2,E", rover.executeCommand("ffrff"));

	}

	@Test
	public void roverMovesBackByOrigin() throws MarsRoverException {

		int pX = 0;
		int pY = 0;
		obstacles.add("5,5");
		obstacles.add("4,7");
		MarsRover rover = new MarsRover(pX, pY, obstacles);
		rover.setRoverDir("N");
		assertEquals("0,9,N", rover.executeCommand("b"));

	}

}
